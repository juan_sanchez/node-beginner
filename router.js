function route(handle, pathname, response, postdata) {
	console.log("A punto de rutear una peticion para " + pathname);
	
	if (typeof handle[pathname] === 'function') {
		handle[pathname](response, postdata);
	} else {
		console.log("No se encontro manipulador para " + pathname);
		handle['/noencontrado'](response);
	}
}
exports.route = route;
