var exec = require("child_process").exec;
var querystring = require("querystring");

function iniciar( response, postdata ) {
	console.log("Manipulador de peticion 'iniciar' fue llamado.");

  var body = '<html>'+
    '<head>'+
    '<meta http-equiv="Content-Type" content="text/html"; charset="UTF-8" />'+
    '</head>'+
    '<body>'+
    '<form action="/subir" method="post">'+
    '<textarea name="text" rows="20" cols="60"></textarea>'+
    '<input type="submit" value="Submit text" />'+
    '</form>'+
    '</body>'+
    '</html>';

    response.writeHead(200, {"Content-Type": "text/html"});
    response.write(body);
    response.end();
}

function subir( response, postdata ) {
	console.log("Manipulador de peticion 'subir' fue llamado.");
	response.writeHead(200, {"Content-Type": "text/html"});
	response.write("Recived data: " + querystring.parse(postdata)["text"] );
	response.end();
}

function noencontrado( response ) {
	console.log("Manipulador de peticion 'subir' fue llamado.");
	response.writeHead(200, {"Content-Type": "text/html"});
	response.write("No Encontrado");
	response.end();
}

exports.iniciar = iniciar;
exports.subir = subir;
exports.noencontrado = noencontrado;
