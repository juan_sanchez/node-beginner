var http = require("http");
var url  = require("url");
var moment  = require("moment");

function iniciar(route, handle)
{

	function onRequest(request, response)
	{
		var postdata = "";
		var pathname = url.parse(request.url).pathname;
		var timestamp = moment().format('MMMM Do YYYY, h:mm:ss a');
		console.log("Server says: Petición para " + pathname + " recibida. | " + timestamp);

		request.setEncoding("utf8");
		
		request.addListener("data", function(chunk) {
			postdata += chunk
			console.log('data recibido "' + chunk + '"');
		});

		request.addListener("end", function() {
			route(handle, pathname, response, postdata)		
		});   
		
	}

	http.createServer(onRequest).listen(8888);
	console.log("Server says: Servidor Iniciado.");

}

exports.iniciar = iniciar;
